# Reporte de Roles
* [Invitado](#invitado)
* [Reportero](#reportero)
* [Desarrollador](#desarrollador)
* [Mantenedor](#mantenedor)
* [General](#general)

## Invitado

### Permisos
* Solo puede hacer sugerencias en la wiki pero no puede crear páginas de wiki.

### Restricciones

* Puede crear issues, y ver discusiones.
* No puede hacer merge requests
* No puede crear ramas
* No puede hacer push


## Reportero

### Permisos
* Puede hacer pull
* Puede hacer issues, y ver discusiones.
* Sugerencias en la wiki

### Restricciones
* No puede hacer merge requests
* No acceso CI \CD
* No puede hacer 

## Desarrollador

### Permisos
* Puede hacer pull
* Puede hacer issues y ver discusiones
* Puede hacer merge requests
* Puede crear ramas
* Puede hacer push a la rama creada
* Acceso a CI\CD
* Crear paginas en wiki

### Restricciones
* No puede hacer push al master


## Mantenedor

### Permisos
* Puede hacer pull
* Crear paginas en la wiki
* Acceso CI\CD
* Acceso a las configuraciones del proyecto
* Cambio en los permisos en los miembros
* Puede autorizar merges

### Restricciones
* El Propietario puede restringirle lo que considere necesario.

## Propietario

* Puede hacer todo.

## General

* No pueden borrar ramas
* No pueden borrar issues
* Todos pueden editar sus issues
* Todos pueden hacer pull

> Para más información acerca de los permisos visite el siguiente [link](https://gitlab.com/help/user/permissions)