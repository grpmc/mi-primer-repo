# Alexander Tigselema

![Foto](https://gitlab.com/grpmc/mi-primer-repo/raw/ramaIssue/ejemplo-folder/foto.jpg)

[Facebook](https://www.facebook.com/alexander.tigselema.7)

Alexander es un fan de los videojuegos tipo survival-horror, la buena literatura y series de TV. Actualmente reside en Quito - Ecuador. Él estudia Ingeniería en Sistemas Informáticos y de Computación en la Escuela Politécnica Nacional con el afán de crear algún videojuego uno de estos días, domina diversos lenguajes de programación y disfruta haciendo experimentos propios con lo que aprende en la universidad.

> "Le tocaron, como a todos los hombres, malos tiempos en que vivir" Esta frase de Jorge Luis Borges siempre se encuentra rondando sus pensamientos.

En su tiempo libre, cuando no está pegado a su computador, Alexander gusta de caminar y observar los paisajes que ofrece su linda ciudad. Hoy en día el se encuentra colaborando con Manticore Labs.